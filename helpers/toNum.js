// This helper, toNum, will sanitize the data/statistic of the countries. It will remove the comma and convert the strings into number.

export default function toNum(str) {

	// this function will receive string and return a number
	// converts a string into an array by using a spread operator.

	const arr = [...str]

	// filter out the commas out of our numerical string
	const filteredArr = arr.filter(element => {

		// iterates over all the elements, items in an array and passes the items that pass a condition into a new array. Unlike find(), filter iterates over all items, but in find() it stops when an item has passed the condition.
		return element !== ","
	})

	// reduce the array into a single string which will be then be parsed as an integer by parseInt()
	return parseInt(filteredArr.reduce((x,y) => {
		
		/*
		reduce:
		
		on the first iteration:
		x is the first item in the array
		y is the second item in the array

		on the second iteration:
		x is the value of the addition in the first iteration
		y is the next item in the array

		note: Strings do not add, instead it concatenates
		*/

		// console.log(x)
		// console.log(y)

		return x + y
	}))

	// orig arr
	// console.log(arr)

	// filtered arr
	// console.log(filteredArr)

}