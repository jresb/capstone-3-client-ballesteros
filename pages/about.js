import {useState,useEffect,useContext, Fragment} from 'react'
import {Form, Jumbotron, Button, Card, Row, Col, Table, CardDeck, Container, Nav} from 'react-bootstrap'
import Swal from 'sweetalert2'
import moment from 'moment'
import UserContext from './../userContext'
import Head from 'next/head'

// to redirect the user, use the router component from next.js
import Router from 'next/router'

export default function About() {
	return (
		<Fragment>
		<Head>
	    	<title>Pence | About</title>
	    	<link rel="icon" href="/favicon.ico" />
	    </Head>
			<h1 className="text-center">About</h1>
			<Row>
				<Col className="my-3" sm={12} md={12} lg={6}>				
					<Card  className="bg-dark text-white mt-1 mx-auto h-100">
					  <Card.Body>
					    <Card.Title>About Pence</Card.Title>
					    <Card.Subtitle className="mb-2 text-muted">A quick description :D</Card.Subtitle>
					    <Card.Text>
					      Pence is a budget tracker app that enables the user to store records of their income and expenses. It is made with MERN (MongoDN, ExpressJS, ReactJS, NodeJS) Stack, NextJS and Bootstrap. It is done with love and tears and blood and sleepless nights.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
				<Col className="my-3" sm={12} md={12} lg={6}>				
					<Card  className="bg-dark text-white mt-1 mx-auto h-100">
						<Card.Body>
						    <Card.Title>About the Developer</Card.Title>
						    <Card.Subtitle className="mb-2 text-muted">Hello!</Card.Subtitle>
						    <Card.Text>
							    <p>I'm Jerico, and I am the developer of Pence! Feel free to contact me through my details:</p>
						    	<p>
						      		Email: jresballesteros@gmail.com
						      	</p>
						      	<p>
						      		Number: +63 917 858 1603
						      	</p>
						    </Card.Text>
						</Card.Body>
				    	<Card.Footer className="text-center">
							<Button className="btn-secondary btn-block" href="https://jresb.gitlab.io/capstone1-ballesteros" target="_blank">Check my portfolio!</Button>
				    		<Button className="btn-secondary btn-block" href="https://gitlab.com/jresb" target="_blank">Gitlab</Button>
	        				<Button className="btn-secondary btn-block" href="https://www.linkedin.com/in/jresballesteros/" target="_blank">LinkedIn</Button>
				    	</Card.Footer>
					</Card>
				</Col>
			</Row>
		</Fragment>
	)
}

