import {Form, Button, Card, Row, Col, Table} from 'react-bootstrap'
import {useState, useEffect, Fragment} from 'react'
import {colorRandomizer} from '../helpers/colorRandomizer'
import {Bar, Pie, Line, Chart} from 'react-chartjs-2'
import randomcolor from 'randomcolor'
import moment from 'moment'
import Head from 'next/head'

export default function balanceOverview({rawData}) {

Chart.defaults.global.defaultFontColor = 'white';

	function onlyUnique(value, index, self) {
		return self.indexOf(value) === index;
	}

	// for monthly income
	const [incomeRec, setIncomeRec] = useState([])
	const [incomeMos, setIncomeMos] = useState([])
	const [incomeBal, setIncomeBal] = useState([])

	// for monthly expense
	const [expenseRec, setExpenseRec] = useState([])
	const [expenseMos, setExpenseMos] = useState([])
	const [expenseBal, setExpenseBal] = useState([])

	// for pie
	const [categories, setCategories] = useState([])
	const [amount, setAmount] = useState([])

	// for budget trend
	const [allRec, setAllRec] = useState([])
	const [allBal, setAllBal] = useState([])

	// for Search Breakdown
	const [searchByMonth, setSearchByMonth] = useState('')
	const [searchByYear, setSearchByYear] = useState('')

	// for setting max value of line graph (wip)
	const [inTemp, setInTemp] = useState('')
	const [exTemp, setExTemp] = useState('')

	useEffect(() => {

		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/record`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setAllRec(data)

			setIncomeRec(data.filter(e => e.type === "Income"))
			setExpenseRec(data.filter(e => e.type === "Expense"))
			
			let Categories = [];
			let expenseCategories = [];
			let Amount = [];

			Categories = data.filter(e => e.type === "Expense")
			
			expenseCategories = Categories.map(e => `${e.category} (${e.description})`)
			Amount = Categories.map(e => e.amount)
			
			setAmount(Amount)
			// setCategories(expenseCategories.filter(onlyUnique))
			setCategories(expenseCategories)
		})
	},[]) 


	useEffect(() => {
		let tempMonths = [] 

		// new ===============================================
		/*
		let a = []
		let b = [] 
		a = incomeRec.map(e => e.date).map(e => e.replace('T00:00:00.000Z','')).map(e => e.split('-')).filter(e => e[0] === searchByYear)
		for (let i=0; i<a.length; i++){
			if (a[i][0] === searchByYear) {
				b.push(i)
			}
		}
		setIncomeRec(b)
		*/
		// new ===============================================

		incomeRec.forEach(elem => {
			if(!tempMonths.find(mos => mos === moment(elem.date).format('MMMM'))) {
				tempMonths.push(moment(elem.date).format('MMMM'))
			}

		const monthsRef = ["January","February","March","April","May","June","July","August","September","October","November","December"]

		tempMonths.sort((a,b) => {
			if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){
				return monthsRef.indexOf(a) - monthsRef.indexOf(b)
			}
		})
		setInTemp(tempMonths) // holder for the number of months that have transactions
		setIncomeMos(monthsRef) //change to tempMonths to display months with transactions, monthsRef to display all months
		})
	},[incomeRec])


	useEffect(() => {
		let tempMonths = []

		// new ===============================================
		/*
		let a = []
		let b = [] 
		a = expenseRec.map(e => e.date).map(e => e.replace('T00:00:00.000Z','')).map(e => e.split('-')).filter(e => e[0] === searchByYear)
		for (let i=0; i<a.length; i++){
			if (a[i][0] === searchByYear) {
				b.push(i)
			}
		}
		setExpenseRec(b)
		*/
		// new ===============================================

		expenseRec.forEach(elem => {
			if(!tempMonths.find(mos => mos === moment(elem.date).format('MMMM'))) {
				tempMonths.push(moment(elem.date).format('MMMM'))
			}

		const monthsRef = ["January","February","March","April","May","June","July","August","September","October","November","December"]

		tempMonths.sort((a,b) => {
			if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){
				return monthsRef.indexOf(a) - monthsRef.indexOf(b)
			}
		})
		setExTemp(tempMonths) // holder for the number of months that have transactions
		setExpenseMos(monthsRef) //change to tempMonths to display months with transactions, monthsRef to display all months
		})
	},[expenseRec])


	useEffect(() => {
		setIncomeBal(incomeMos.map(mos => {
			let gain = 0;
			incomeRec.forEach(elem => {
				if(moment(elem.date).format("MMMM") === mos){
					gain += parseInt(elem.amount)
				}
			})
			return gain
		}))

		setExpenseBal(expenseMos.map(mos => {
			let loss = 0;
			expenseRec.forEach(elem => {
				if(moment(elem.date).format("MMMM") === mos){
					loss += parseInt(elem.amount)
				}
			})
			return loss
		}))
	},[incomeMos,expenseMos])

	useEffect(() => {
		let newBal = allRec.map(bal => {
			return bal.balance
		})
		setAllBal(newBal)
	},[allRec])

	function dateRange(e) {

		e.preventDefault()

		let year = parseInt(searchByYear)
		let month = searchByMonth.toLowerCase()
		let months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
		months = months.map(e => e.toLowerCase())

		// creates an array that contains only the dates wihtout the time
		let dates = allRec.map(e => e.date).map(e => e.replace('T00:00:00.000Z',''))

		// creates an array that contains the year, month, and date within an array
		let a = []
		a = dates.map(e => e.split('-'))

		// provides an array of indexes that match the search criteria
		let b = []
		for (let i=0; i<a.length; i++){
			if (parseInt(a[i][0]) === year && parseInt(a[i][1]) === (months.indexOf(month) + 1)) {
				b.push(i)
			}
		}

		let result = []
		for (let i=0; i<b.length; i++){
			result.push(allRec[b[i]]) 
		}
		// return result

		if (months.includes(month)) {		
			result = result.filter(e => e.type === "Expense")
			
			setAllBal(result.map(e => e.balance))
			setAmount(result.map(e => e.amount))
			setCategories(result.map(e => `${e.category} (${e.description})`))
		} else {
			result = allRec.filter(e => e.type === "Expense")
			
			setAllBal(result.map(e => e.balance))
			setAmount(result.map(e => e.amount))
			setCategories(result.map(e => `${e.category} (${e.description})`))
		}

		if (result.length === 0) {
			setAllBal([0])
			setAmount([100])
			setCategories([`No expenses for ${searchByMonth}, ${searchByYear}`])
		}
	}

	// monthly income
	const data1 = {
		labels: incomeMos,
		datasets: [{
			label: 'Income',
			backgroundColor: 'blue',
			borderColor: 'black',
			borderWidth: 1,
			barThickness:50,
			hoverBackgroundColor: 'darkblue',
			hoverBorderColor: 'black',
			data: incomeBal
		}]
	}

	// monthly expense
	const data2 = {
		labels: expenseMos,
		datasets: [{
			label: 'Expense',
			backgroundColor: 'red',
			borderColor: 'black',
			borderWidth: 1,
			barThickness:50,
			hoverBackgroundColor: 'darkred',
			hoverBorderColor: 'black',
			data: expenseBal
		}]
	}

	let colors = categories.map(e => randomcolor())

	// pie
	const data3 = {
		labels: categories,
		datasets: [{
			backgroundColor: colors,
			data: amount
		}]
	}

	// budget trend
	const data4 = {
		labels: allBal,
		datasets: [{
			label: 'Income vs Expenses',
			backgroundColor: '#FCFC92',
			borderColor: 'black',
			data: allBal
		}]
	}

	const options4 = {
		options: {
				responsive: true,
				title: {
					display: true,
					text: 'Budget Trend'
				},
				tooltips: {
					mode: 'index',
				},
				hover: {
					mode: 'index'
				},
				scales: {
					xAxes: [{
						scaleLabel: {
							display: true,
							labelString: 'Records'
						}
					}],
					yAxes: [{
						stacked: true,
						scaleLabel: {
							display: true,
							labelString: 'Balance'
						}
					}]
				},
				legend: {
					labels:{
						fontColor: 'yellow'
					}
				}
			}
	}

	const options = {
		scales: {
			yAxes: [
				{
					ticks: {
						beginAtZero: true,
						// max: max
					}
				}
			]
		},
		responsive: true
	}

	return (
		<Fragment>
		<Head>
	    	<title>Pence | Balance Overview</title>
	    	<link rel="icon" href="/favicon.ico" />
	    </Head>
			<Row className="my-3">
				<Col sm={12} md={12} lg={6} className="mb-3">				
					<Card className="bg-dark text-white mx-auto h-100" style={{ width: '18rem' }}>
					  <Card.Body>
					    <Card.Title style={{fontSize: 20}}>Search Breakdown</Card.Title>
							<Form onSubmit = {e => dateRange(e)}>
					    	
					    	<Form.Group>
					    		<Form.Label>Sort Transactions by Month</Form.Label>
					    		<Form.Control as="select" value={searchByMonth} onChange={e => setSearchByMonth(e.target.value)}>
					    			<option value="" disabled>Select Month</option>
						    		<option>All</option>
						    		<option>January</option>
						    		<option>February</option>
						    		<option>March</option>
						    		<option>April</option>
						    		<option>May</option>
						    		<option>June</option>
						    		<option>July</option>
						    		<option>August</option>
						    		<option>September</option>
						    		<option>October</option>
						    		<option>November</option>
						    		<option>December</option>
						    	</Form.Control>
					    	</Form.Group>

					    	<Form.Group>
					    		<Form.Label>Sort Transaction by Year</Form.Label>
					    		<Form.Control type="number" placeholder="Enter Year" value={searchByYear} onChange={e => setSearchByYear(e.target.value)} required />
					    	</Form.Group>

						    <Button type="submit" className="btn-secondary btn">Submit</Button>
					    	</Form>
					  </Card.Body>
					</Card>
				</Col>
				<Col sm={12} md={12} lg={6}>
					<h3 className="text-center">Category Overview</h3>
					<Pie data={data3} className=""/>
				</Col>
			</Row>

			<Row>
				{
					searchByYear === ''
					?
					<Fragment>					
						<Col sm={12} md={12} lg={6}>
							<h3 className="text-center">Monthly Income</h3>
							<Bar data={data1} options={options}/>
						</Col>
						<Col sm={12} md={12} lg={6}>
							<h3 className="text-center">Monthly Expense</h3>
							<Bar data={data2} options={options}/>
						</Col>
					</Fragment>
					:
					<Fragment>
						<Col sm={12} md={12} lg={6}>
							<h3 className="text-center">Monthly Income for {searchByYear}</h3>
							<Bar data={data1} options={options}/>
						</Col>
						<Col sm={12} md={12} lg={6}>
							<h3 className="text-center">Monthly Expense for {searchByYear}</h3>
							<Bar data={data2} options={options}/>
						</Col>
					</Fragment>
				}
			</Row>

			<Row>
				<h3 className="text-center mt-5 mx-auto">Budget Trend</h3>
				<Line data={data4} options={options4} />
			</Row>
		</Fragment>
	)
}