import {useState,useEffect,useContext,Fragment} from 'react'
import {Form, Jumbotron, Button, Card, Row, Col, Table, Container} from 'react-bootstrap'
import Swal from 'sweetalert2'
import {Doughnut, Chart} from 'react-chartjs-2'
import Image from 'next/image'

import Head from 'next/head'

import UserContext from './../userContext'

// to redirect the user, use the router component from next.js
import Router from 'next/router'

export default function Profile() {

	Chart.defaults.global.defaultFontColor = 'white';

	const {user} = useContext(UserContext)
	const [userName, setUserName] = useState('')
	const [userMail, setUserMail] = useState('')
	const [balance, setBalance] = useState('')
	const [totalIncome, setTotalIncome] = useState('')
	const [totalExpense, setTotalExpense] = useState('')

	const [transactions, setTransactions] = useState('')
	const [incRec, setIncRec] = useState('')
	const [expRec, setExpRec] = useState('')

	useEffect(() => {
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/details`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setUserName(`${data.firstName} ${data.lastName}`)
			setBalance(data.balance)
			setUserMail(data.email)

			setTransactions(data.records.length)
			setIncRec(data.records.filter(e => e.type === "Income").length)
			setExpRec(data.records.filter(e => e.type === "Expense").length)

			let income = data.records.filter(e => e.type === "Income")
			setTotalIncome(income.map(e => e.amount).reduce((x,y) => x + y))
			let expense = data.records.filter(e => e.type === "Expense")
			setTotalExpense(expense.map(e => e.amount).reduce((x,y) => x + y))


		})
	},[])

	const data = {
		labels: ["Income", "Expense"],
		datasets: [{
			backgroundColor: ["blue","red"],
			borderColor: 'white',
			data: [totalIncome,totalExpense]
		}]
	}

	let userNote;
	let noteCol;
	if (totalExpense > totalIncome) {
		let x = totalExpense - totalIncome
		noteCol = "text-danger text-center"
		userNote = `WARNING: You're balance has turned negative. Expenses are greater by ₱${x.toLocaleString()}.`
	} else if (totalExpense < totalIncome){
		let x = totalIncome-totalExpense
		noteCol = "text-success text-center"
		userNote = `Income greater than Expenses by ₱${x.toLocaleString()}. Keep it up!`
	}

	return (
		<Container className="for-profile">
		<Head>
			<title>
				Pence | Profile
			</title>
			<link rel="icon" href="/favicon.ico" />

		</Head>
			<h1 className="text-center">Profile</h1>
				
			<Row>		
		  		<Col sm={12} md={12} lg={6} className="my-2">  			
					<Card className="bg-dark text-white mx-auto h-100 mt-1">
					  <Card.Body>
					    <Card.Title className="" style={{fontSize: 40}}>{userName}</Card.Title>
					    <Card.Subtitle className="mb-2 text-muted">{userMail}</Card.Subtitle>
							    <p><strong>Balance: </strong> ₱{parseInt(balance).toLocaleString()}<span>{<br/>}</span>
							    <strong>Total Income: </strong> ₱{totalIncome.toLocaleString()}<span>{<br/>}</span>
							    <strong>Total Expenses: </strong> ₱{totalExpense.toLocaleString()}<span>{<br/>}</span>
							    <strong>No. of Total Transactions: </strong> {transactions}<span>{<br/>}</span>
							    <strong>No. of Income Records: </strong> {incRec}<span>{<br/>}</span>
							    <strong>No. of Expense Records: </strong> {expRec}<span>{<br/>}</span>
							    </p>
							    <p className={noteCol}>
								    {userNote}
							    </p>
					  </Card.Body>
					</Card>
				</Col>
				<Col sm={12} md={12} lg={6} className="my-2">
					<Card className="bg-dark text-white mx-auto h-100 mt-1">
					  <Card.Body>
					    <Card.Title className="mb-3" style={{fontSize: 40}}>Income vs Expense</Card.Title>
							<Doughnut data={data} />
							<p className="text-center mt-3">
								<span className="text-primary">Income: {((totalIncome/(totalIncome+totalExpense))*100).toFixed(2)}%</span><span>{<br/>}</span>
								<span className="text-danger">Expense: {((totalExpense/(totalIncome+totalExpense))*100).toFixed(2)}%</span>
							</p>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Container className="text-center">
				<Image className="mx-auto mt-4"
				src="/profile.svg"
				alt="profile_page_svg"
				width={1235*0.6}
				height={858*0.6}
				/>
			</Container>
		</Container>
	)
}
