import {useState,useEffect,useContext,Fragment} from 'react'
import {Card, Form, Jumbotron, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import Head from 'next/head'

import UserContext from './../userContext'

// to redirect the user, use the router component from next.js
import Router from 'next/router'

export default function Register() {

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")

	const [isActive, setIsActive] = useState(true)

	useEffect(() => {

		if ((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length === 11 )) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[firstName,lastName,email,mobileNo,password1,password2])

	function registerUser(e) {
		e.preventDefault();
		// console.log('isActive:',isActive)

		if(isActive) {
			fetch('https://sheltered-cliffs-72072.herokuapp.com/api/users/email-exists', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify ({
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				// console.log(`email exists?: ${data}`)

				if (data === false) {
					fetch('https://sheltered-cliffs-72072.herokuapp.com/api/users/', {
						method: 'POST',
						headers: { 'Content-Type': 'application/json' },
						body: JSON.stringify ({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password1
						})
					})
					.then(res => res.json())
					.then(data => {
						// console.log(data)
						if (data === true) {
							setFirstName("")
							setLastName("")
							setEmail("")
							setMobileNo(undefined)
							setPassword1("")
							setPassword2("")

							Swal.fire({
								icon: "success",
								title: "Registration Successful.",
								text: "Thank you for registering!"
							})
							Router.push('/login')
						} else {
							Swal.fire({
								icon: "error",
								title: "Registration Failed.",
								text: "Something went wrong :("
							})
						}
					})
				} else {
					Swal.fire({
						icon: "error",
						title: "Registration Failed.",
						text: "Email already exists."
					})
				}
			})
		}
	}

	return (
		<Fragment>
		<Head>
	    	<title>Pence | Register</title>
	    	<link rel="icon" href="/favicon.ico" />
	    </Head>
			<Card className="bg-dark text-white my-3 h-100 mx-auto" style={{ width: '18rem' }}>
			  <Card.Body>
			    <Card.Title className="text-center" style={{fontSize: 30}}>Register</Card.Title>
					<Form onSubmit = {e => registerUser(e)}>
						<Form.Group controlId="userFirstName">
							<Form.Label>First Name</Form.Label>
							<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required />
						</Form.Group>
						<Form.Group controlId="userLastName">
							<Form.Label>Last Name</Form.Label>
							<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required />
						</Form.Group>
						<Form.Group controlId="userEmail">
							<Form.Label>Email</Form.Label>
							<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required />
						</Form.Group>
						<Form.Group controlId="userMobileNo">
							<Form.Label>Mobile No.</Form.Label>
							<Form.Control type="number" placeholder="Enter Mobile No." value={mobileNo} onChange={e => setMobileNo(e.target.value)} required />
						</Form.Group>
						<Form.Group controlId="userPassword1">
							<Form.Label>Password</Form.Label>
							<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required />
						</Form.Group>
						<Form.Group controlId="userPassword2">
							<Form.Label>Confirm Password</Form.Label>
							<Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={e => setPassword2(e.target.value)} required />
						</Form.Group>
						<Button className="mx-auto btn-block" variant="secondary" type="submit" disabled={isActive == false}>Register</Button>
					</Form>
			  </Card.Body>
			</Card>
		</Fragment>
	)
}