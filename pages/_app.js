import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import {useState,useEffect,Fragment} from 'react'
import {UserProvider} from '../userContext'
import Head from 'next/head'

// import components
import NavBar from '../components/NavBar'
import Footer from '../components/Footer'
// import BS components
import {Container} from 'react-bootstrap'

function MyApp({ Component, pageProps }) {

const [user, setUser] = useState({email: null, id: null})

useEffect(() => {
	setUser({
		email: localStorage.getItem('email'),
		id: localStorage.getItem('id')
	})
},[])

const unsetUser = () => {
	localStorage.clear();
	setUser({email: null, id: null});
}

  return (
  	<Fragment>
	  	<Head>
	  		<link rel="preconnect" href="https://fonts.gstatic.com" />
  			<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200&family=Quicksand:wght@300&display=swap" rel="stylesheet" />
	  	</Head>
  		<UserProvider value={{user,setUser,unsetUser}}>
	  		<NavBar />
		  	<Container>
			  	<Component {...pageProps} />
		  	</Container>
		  	{/*<Footer />*/}
  		</UserProvider>
  	</Fragment>
  	)
}

export default MyApp
