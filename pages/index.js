import {Fragment,useContext} from 'react'
import {Jumbotron, Button, Card, Container, Row, Col} from 'react-bootstrap'
import toNum from './../helpers/toNum'
import UserContext from './../userContext'
import Image from 'next/image'
import Head from 'next/head'

export default function Home() {

	const {user} = useContext(UserContext)

  return (
    <Fragment>
	    <Head>
	    	<title>Pence | Home</title>
	    	<link rel="icon" href="/favicon.ico" />
	    </Head>
		<Card className="bg-dark text-white mt-1 mx-auto h-100" id="indexCard">
		  <Card.Body>
		    <Card.Title className="text-center" style={{fontSize:30}}>Welcome to Pence</Card.Title>
		    <Card.Subtitle className="mb-2 text-muted text-center">Your personal budget tracker for your daily transactions.</Card.Subtitle>
		    {
		    	user.email
		    	?
		    	<Fragment>
			    	<Card.Text className="mx-4">
			    		Hello! How's it going? Let's get started shall we? You can try these: <span>{<br/>}{<br/>}</span>
			    		1. Start by adding categories. You can go to <strong>Categories</strong> in the <strong>Actions</strong> dropdown.<span>{<br/>}</span>
			    		2. Name your category and choose its type: <strong>Income</strong> or <strong>Expense</strong>.<span>{<br/>}</span>
			    		3. Go to <strong>Records</strong> and add your transaction. You can only choose categories that you've added.<span>{<br/>}</span>
			    		4. Check <strong>Balance Overview</strong> to visualize your transactions. You can also sort transactions by month.<span>{<br/>}</span>
			    		5. Click <strong>Search</strong> in the upper right corner to search your records and categories.<span>{<br/>}</span>
			    		6. Have fun and spend wisely!

			    	</Card.Text>
		    	<Container className="text-center">
			    	<Button className="btn-secondary mx-3" href="/categories">Add categories</Button>
			    	<Button className="btn-secondary mx-3" href="/about">About</Button>
		    	</Container>
		    	</Fragment>
		    	:
		    	<Container className="text-center">
		    		<Card.Text className="text-center">
			    		Obtain full control of your finances and never lose track of your income and expenses with Pence! Login to get started.
			    	</Card.Text>
			    	<Button className="btn-secondary mx-3" href="/login">Let's go!</Button>
			    	<Button className="btn-secondary mx-3" href="/about">About</Button>
		    	</Container>
		    }
		  </Card.Body>
		</Card>
		<Container className="text-center mt-4">
		{/*
			<Row>
				<Col sm={12} md={6} lg={6}>
					<Image className="mx-auto"
					src="/pence.png"
					alt="pence_logo"
					width={1115}
					height={279}
					/>
				</Col>
				<Col sm={12} md={6} lg={6}>			
					<Image className="mx-auto"
					src="/home.svg"
					alt="home_page_svg"
					width={1235*0.6}
					height={858*0.6}
					/>
				</Col>
			</Row>
		*/}
			<Image className="mx-auto"
				src="/home.svg"
				alt="home_page_svg"
				width={1235*0.6}
				height={858*0.6}
			/>
		</Container>
    </Fragment>
  )
}