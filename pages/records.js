import {useState,useEffect,useContext, Fragment} from 'react'
import {Form, Jumbotron, Button, Card, Row, Col, Table, InputGroup} from 'react-bootstrap'
import Swal from 'sweetalert2'
import moment from 'moment'
import UserContext from './../userContext'
import Head from 'next/head'

// to redirect the user, use the router component from next.js
import Router from 'next/router'

export default function Records() {

	// sets an array of only unique elements
	function onlyUnique(value, index, self) {
		return self.indexOf(value) === index;
	}
	
	const {user} = useContext(UserContext)
	const [records, setRecords] = useState([])
	const [balance, setBalance] = useState('')
	const [type, setType] = useState('')
	const [category, setCategory] = useState([])
	const [amount, setAmount] = useState('')
	const [date, setDate] = useState('')
	const [description, setDescription] = useState('')
	const [newCategory, setNewCategory] = useState([])
	
	const [filter, setFilter] = useState('')
	const [incRec, setIncRec] = useState([])
	const [expRec, setExpRec] = useState([])
	const [allRec, setAllRec] = useState([])



	// get balance
	useEffect(() => {
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/balance`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => setBalance(data))
	},[balance])

	// dropdown for categories list with get all categories
	useEffect(() => {
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/category`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			let incCatArr = [];
			let expCatArr = [];

			if (type === "Income") {
				data.filter(inc => {
				    if (inc.type === "Income") {
				    	incCatArr.push({_id: inc._id, name: inc.name})
				    } 
				})
				setNewCategory(incCatArr)
			} else if (type === "Expense") {
				data.filter(exp => {
				    if (exp.type === "Expense") {
				    	if (!expCatArr.includes(exp.name)) {
					    	expCatArr.push({_id: exp._id, name: exp.name})
				    	}
				    } 
				})
				setNewCategory(expCatArr)
			}
		})
	}, [type])

	let res = newCategory
	// let res = newCategory.filter(onlyUnique).sort()
	const categoryDropDown = res.map(categories => {
		return (
			<option key={categories._id}>{categories.name}</option>
		)
	})

	// get all records
	useEffect(() => {
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/record`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => setAllRec(data))
	},[])

	//function in form submit (add Record)
	function addRecord(e) {
		e.preventDefault();

		let amt = parseInt(amount);
		let newBalance;

		if(type === "Income"){
			newBalance = balance + amt
		} else {
			newBalance = balance - amt
		}

		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/addRecord`, {
			method: 'POST',
			headers: {
				'Content-Type' :'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify ({
				id: user.id,
				type: type,
				category: category,
				amount: amount,
				date: date,
				description: description,
				balance: newBalance
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			fetch('https://sheltered-cliffs-72072.herokuapp.com/api/users/addBalance', {
				method: 'PUT',
				headers: {
					'Content-Type' :'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify ({
					id: user.id,
					balance: newBalance
				})
			})
			.then(res => res.json())
			.then(data => {
				setBalance(newBalance)
			})
			if (data) {
				Swal.fire({
					icon: "success",
					title: "Add Record Successful",
					text: `${category} added.`
				})
				// Router.reload('/records')
				windows.replace.location('/records')
			} else {
				Swal.fire({
					icon: "error",
					title: "Oops!",
					text: `Something went wrong :c`
				})
			}
		})
	}

	useEffect(() => {
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/income`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => setIncRec(data))

		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/expense`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => setExpRec(data))
	},[])

	useEffect(() => {
		if (filter === "All") {
			setRecords(allRec)
		} else if (filter === "Income") {
			setRecords(incRec)
		} else if (filter === "Expense"){
			setRecords(expRec)
		}
	},[filter])

	// table for records
	const recordRows = records.map(records => {
		let date = moment(records.date).format("LL")
		return (
			<tr key={records._id} className="text-center">
				<td>{records.type}</td>
				<td>{records.category}</td>
				<td>{records.description}</td>
				<td>{records.amount}</td>
				<td>{date}</td>
			</tr>
		)
	})


	return (
		<Fragment>
		<Head>
	    	<title>Pence | Records</title>
	    	<link rel="icon" href="/favicon.ico" />
	    </Head>
			<h1 className="text-center">Records</h1>
			<h5 className="text-white text-center">Balance: PHP {balance}</h5>
			<Row>
				<Col sm={12} md={12} lg={12} xl={6}>				
					<Card bg="dark" text="light" className="my-3 mx-auto" style={{ width: '18rem' }}>
					  <Card.Body>
					    <Card.Title>Add Record</Card.Title>

					    <Form onSubmit = {e => addRecord(e)}>
					    	<Form.Group>
					    		<Form.Label>Type</Form.Label>
					    		<Form.Control as="select" value={type} onChange={e => setType(e.target.value)}>
					    			<option value="" disabled>Select Type</option>
						    		<option>Income</option>
						    		<option>Expense</option>
						    	</Form.Control>
					    	</Form.Group>

					    	<Form.Group>
					    		<Form.Label>Category</Form.Label>
					    		<Form.Control as="select" value={category} onChange={e => setCategory(e.target.value)} required>
					    			<option value="" disabled>Select Category</option>
					    			{categoryDropDown}
					    		</Form.Control>
					    	</Form.Group>

					    	<Form.Group>
					    		<Form.Label>Amount</Form.Label>
					    		<Form.Control type="number" placeholder="eg. 10000" value={amount} onChange={e => setAmount(e.target.value)} required />
					    	</Form.Group>
					    	<Form.Group>
					    		<Form.Label>Description</Form.Label>
					    		<Form.Control type="text" placeholder="eg. Trip to UK." value={description} onChange={e => setDescription(e.target.value)} required />
					    	</Form.Group>
					    	<Form.Group>
					    		<Form.Label>Date</Form.Label>
					    		<Form.Control type="date" placeholder="Set date" value={date} onChange={e => setDate(e.target.value)} required />
					    	</Form.Group>
						    <Button type="submit" className="btn-secondary btn">Submit</Button>
					    </Form>

						</Card.Body>
					</Card>
				</Col>

				<Col className="my-3">
		
					<InputGroup className="mb-3">
				    	<InputGroup.Prepend>
				     		<InputGroup.Text>Filter</InputGroup.Text>
				    	</InputGroup.Prepend>
				    	<Form.Control as="select" value={filter} onChange={ e => setFilter(e.target.value)}>
				 			<option>Select Type</option>
				    		<option>All</option>
				    		<option>Income</option>
				    		<option>Expense</option>
				    	</Form.Control>
				  	</InputGroup>

					<Table responsive="sm" size="md" striped bordered hover variant="dark" className="mx-auto">
						<thead>
							<tr className="text-center">
								<th>Type</th>
								<th>Category</th>
								<th>Description</th>
								<th>Amount</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							{recordRows}
						</tbody>
					</Table>
				</Col>

			</Row>
		</Fragment>
	)
}