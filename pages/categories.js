import {useState,useEffect,useContext,Fragment} from 'react'
import {Form, Jumbotron, Button, Card, Row, Col, Table} from 'react-bootstrap'
import Swal from 'sweetalert2'
import Head from 'next/head'

import UserContext from './../userContext'

// to redirect the user, use the router component from next.js
import Router from 'next/router'

export default function Categories() {

	const {user} = useContext(UserContext)
	const [records, setRecords] = useState([])
	const [balance, setBalance] = useState('')
	const [type, setType] = useState('')
	const [category, setCategory] = useState([])
	const [income, setIncome] = useState([])
	const [expense, setExpense] = useState([])
	const [categoryName, setCategoryName] = useState('')
	const [amount, setAmount] = useState('')
	const [description, setDescription] = useState('')

	// get balance
	useEffect(() => {
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/balance`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => setBalance(data))
	},[balance])

	// get all categories
	useEffect(() => {
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/category`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => setCategory(data))
	},[])

	// get all income categories
	useEffect(() => {
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/income`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => setIncome(data))
	},[income])

	//get all expense categories
	useEffect(() => {
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/expense`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => setExpense(data))
	},[expense])

	// table for income
	const incomeRows = category.map(income => {
		if (income.type === "Income") {
			return (
				<tr key={income._id} className="text-center">
					<td>{income.type}</td>
					<td>{income.name}</td>
				</tr>
			)
		}
	})

	// table for expense
	let arrExp = [];
	const expenseRows = category.map(expense => {
		if (expense.type === "Expense") {
			arrExp.push(expense.name)
			return (
				<tr key={expense._id} className="text-center">
					<td>{expense.type}</td>
					<td>{expense.name}</td>
				</tr>
			)
		}
	})

	// get all records
	useEffect(() => {
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/record`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => setRecords(data))
	},[records])

	function addCategory(e) {
		e.preventDefault()

		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/addCategory`, {
			method: 'POST',
			headers: {
				'Content-Type' :'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify ({
				id: user.id,
				name: categoryName,
				type: type
			})
		})
		.then(res => res.json())
		.then(data => {
    		if(data) {
					setCategoryName('');
					setType('');
					Swal.fire({
						icon: "success",
						title: `${categoryName} added successfully.`,
						text: "Success"
					})
					Router.reload('/categories');
				} else {
					Swal.fire({
						icon: "error",
						title: `${categoryName} can't be added.`,
						text: "Something went wrong."
					})
				}
		})
	}

	return (
		<Fragment>
		<Head>
	    	<title>Pence | Categories</title>
	    	<link rel="icon" href="/favicon.ico" />
	    </Head>
			<h1 className="text-center">Categories</h1>
			<Row>
				<Col sm={12} md={12} lg={4}>				
					<Card bg="dark" text="light" className="my-3 mx-auto" style={{ width: '18rem' }}>
					  <Card.Body>
					    <Card.Title>Add Category</Card.Title>
					    <Form onSubmit = {e => addCategory(e)}>
					    	<Form.Group>
					    		<Form.Label>Type</Form.Label>
					    		<Form.Control as="select" value={type} onChange={e => setType(e.target.value)}>
						    		<option value="" disabled>Select Type</option>
						    		<option>Income</option>
						    		<option>Expense</option>
						    	</Form.Control>
					    	</Form.Group>
					    	<Form.Group>
					    		<Form.Label>Name</Form.Label>
					    		<Form.Control type="text" placeholder="Category" value={categoryName} onChange={e => setCategoryName(e.target.value)} required/>
					    	</Form.Group>

						    <Button type="submit" className="btn-secondary btn">Submit</Button>
					    </Form>
						</Card.Body>
					</Card>
				</Col>

				<Col sm={12} md={6} lg={4}>
				<h5 className="text-center">Income</h5>
					<Table striped bordered hover variant="dark">
						<thead>
							<tr className="text-center">
								<th>Type</th>
								<th>Category</th>
							</tr>
						</thead>
						<tbody>
							{incomeRows}
						</tbody>
					</Table>
				</Col>

				<Col sm={12} md={6} lg={4}>
				<h5 className="text-center">Expense</h5>
					<Table striped bordered hover variant="dark">
						<thead>
							<tr className="text-center">
								<th>Type</th>
								<th>Category</th>
							</tr>
						</thead>
						<tbody>
							{expenseRows}
						</tbody>
					</Table>
				</Col>

			</Row>
		</Fragment>
	)
}




/*
function onlyUnique(value, index, self) {
		return self.indexOf(value) === index;
	}

data = [
    {
        "_id": "604e7c92ed012716540374ba",
        "name": "Payday",
        "type": "Income"
    },
    {
        "_id": "604e7ca6ed012716540374bb",
        "name": "Travel",
        "type": "Expense"
    },
    {
        "_id": "604e7cb3ed012716540374bc",
        "name": "Bills",
        "type": "Expense"
    },
    {
        "_id": "604e7cbced012716540374bd",
        "name": "Shopping",
        "type": "Expense"
    },
    {
        "_id": "604fb248a4aaac4f2816fe26",
        "name": "Selling",
        "type": "Income"
    },
    {
        "_id": "604fd8f765a4bd20c412894a",
        "name": "Bills",
        "type": "Expense"
    }
]

a = data.map(e => e.name)
// ["Payday", "Travel", "Bills", "Shopping", "Selling", "Bills"]
b = a.filter(onlyUnique)

let a = []
let b = []
for (let i=0; i < data.length ; i++) {
    if (!a.includes(data[i].name)){
    	a.push(data[i].name)    
    }
}

for (let i=0; i < data.length; i++) {
    if (!b.includes(data[i].name)){
    	b.push({name: data[i].name, type: data[i].type})    
    }
}
*/