import {useState,useEffect,useContext,Fragment} from 'react'
import {Card, Form, Jumbotron, Button, Container} from 'react-bootstrap'
import Swal from 'sweetalert2'
import Head from 'next/head'

// to redirect the user, use the router component from next.js
import Router from 'next/router'
import Link from 'next/link'

import UserContext from './../userContext'

import {GoogleLogin} from 'react-google-login'

export default function Login() {

	const {user,setUser} = useContext(UserContext)

	const [email,setEmail] = useState("");
	const [password,setPassword] = useState("");

	const [isActive, setIsActive] = useState(true)

	useEffect(() => {
		if (email !== "" & password !== "") {
			setIsActive(false)
		} else {
			setIsActive(true)
		}
	},[email,password])

	function userLogin(e) {
		e.preventDefault()

		fetch('https://sheltered-cliffs-72072.herokuapp.com/api/users/login', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data.accessToken) {
				localStorage.setItem('token',data.accessToken)
				fetch('https://sheltered-cliffs-72072.herokuapp.com/api/users/details', {
					headers: {
						'Authorization' : `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					localStorage.setItem('email',data.email)
					localStorage.setItem('id',data._id)
					setUser({
						email: data.email,
						id: data._id
					})
					if (data) {					
						Swal.fire({
							icon: "success",
							background: '#FFFFFF',
							title: "Login Successful",
							text: `${email} has logged in.`
						})
						Router.push('/')
					} else {
						Swal.fire({
							icon: "error",
							background: '#FFFFFF',
							title: "Login Failed",
							text: `Something went wrong.`
						})
					}
				})
			}
		})
	}

	function authenticateGoogleToken(response){

		//response - google's response with our tokenId to be used to authenticate our google login user
		//response comes from google along with our user's details and token for authentication.
		// console.log(response)
		
		//send our google login user's token id to the API server for 
		//authentication and retrieval of our own App's token to be used for logging into our App.

		//at this point, this URL endpoint: /verify-google-id-token hasn't been created yet.
		//Refer to part 2 of this Google Login step by step our server side.

		fetch('https://sheltered-cliffs-72072.herokuapp.com/api/users/verify-google-id-token',{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json'

			},
			body: JSON.stringify({

				tokenId: response.tokenId

			})

		})
		.then(res => res.json())
		.then(data => {
			//Refer to part 2 of this Google Login step by step our server side.

			//After receiving the return from our API server:

			//we will show alerts to show if the user logged in properly or if there are errors.
			if(typeof data.accessToken !== 'undefined'){

				//set the accessToken into our localStorage as token:
				localStorage.setItem('token', data.accessToken)

				//run a fetch request to get our user's details and update our global user state and save our user details into the localStorage:
				fetch('https://sheltered-cliffs-72072.herokuapp.com/api/users/details',{

					headers: {

							'Authorization': `Bearer ${data.accessToken}`
					}

				})
				.then(res => res.json())
				.then(data => {

					localStorage.setItem('email', data.email)
					localStorage.setItem('id', data._id)

					//after getting the user's details, update the global user state:
					setUser({

						email: data.email,
						id: data._id

					})

					//Fire a sweetalert to inform the user of successful login:
					Swal.fire({

						icon: 'success',
						title: 'Successful Login'

					})

					Router.push('/categories')
				})

			} else {

				//if data.accessToken is undefined, therefore, data contains a property called error instead.

				//This error will be shown if somehow our user's google token has an error or is compromised.
				if(data.error === "google-auth-error"){

					Swal.fire({

						icon: 'error',
						title: 'Google Authentication Failed'


					})

				} else if(data.error === "login-type-error"){

					//This error will be shown if our user has already created an account in our app using the register page but is trying to use google login to log into our app:

					Swal.fire({

						icon: 'error',
						title: 'Login Failed.',
						text: 'You may have registered through a different procedure.'

					})

				}

			}


		})

	}

	return (
		<Fragment>
		<Head>
	    	<title>Pence | Login</title>
	    	<link rel="icon" href="/favicon.ico" />
	    </Head>
				<Card className="bg-dark text-white my-3 h-100 mx-auto" style={{ width: '18rem' }}>
				  <Card.Body>
				    <Card.Title className="text-center" style={{fontSize: 30}}>Login</Card.Title>
					<Form onSubmit={e => userLogin(e)}>
		                <Form.Group controlId="email">
		                    <Form.Label>Email:</Form.Label>
		                    <Form.Control type="text" placeholder="Enter your email" value={email} onChange={e => setEmail(e.target.value)} required />
		                </Form.Group>
		                <Form.Group controlId="password">
		                    <Form.Label>Password:</Form.Label>
		                    <Form.Control type="password" placeholder="Enter you password" value={password} onChange={e => setPassword(e.target.value)} required />
		                </Form.Group>
		                	
	                	<Button variant="secondary" type="submit" className="mx-auto btn-block">Submit</Button>
						<GoogleLogin
							clientId="940772500577-hacsmesad01n4cgnb210iepmi72r93u8.apps.googleusercontent.com"
							buttonText="Login Using Google"
							onSuccess={authenticateGoogleToken}
							onFailure={authenticateGoogleToken}
							cookiePolicy={'single_host_origin'}
							className="w-100 text-center mt-2 justify-content-center"
						/>
		            </Form>
				  </Card.Body>
				</Card>
			<Container className="text-center">
            	<a href="/register" className="mx-auto text-white" style={{fontSize: 14}}>Not yet Registered? Register Here.</a>
			</Container>
		</Fragment>
	)
}
