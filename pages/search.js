import {useState,useEffect,useContext, Fragment} from 'react'
import {Form, Jumbotron, Button, Card, Row, Col, Table} from 'react-bootstrap'
import Swal from 'sweetalert2'
import moment from 'moment'
import UserContext from './../userContext'
import Head from 'next/head'

// to redirect the user, use the router component from next.js
import Router from 'next/router'

export default function Search() {

	const {user} = useContext(UserContext)

	const [records, setRecords] = useState([])

	const [allRecords, setAllRecords] = useState([])
	const [catRecords, setCatRecords] = useState([])
	const [descRecords, setDescRecords] = useState([])

	const [category, setCategory] = useState('')
	const [description, setDescription] = useState('')

	function searchCategory(e) {
		e.preventDefault()
		
		// get records by category
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/SearchByCategory/${category}`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setRecords(data)
		})
	}

	function searchDescription(e) {
		e.preventDefault()

		// get records by description
		fetch(`https://sheltered-cliffs-72072.herokuapp.com/api/users/searchByDescription/${description}`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setRecords(data)
		})
	}

	const recordRows = records.map(records => {
		let date = moment(records.date).format("LL")
		return (
			<tr key={records._id} className="text-center">
				<td>{records.type}</td>
				<td>{records.category}</td>
				<td>{records.description}</td>
				<td>{records.amount}</td>
				<td>{date}</td>
			</tr>
		)
	})

	return (
		<Fragment>
		<Head>
	    	<title>Pence | Search</title>
	    	<link rel="icon" href="/favicon.ico" />
	    </Head>
			<h1 className="text-center">Search</h1>
			<Row>
				<Col sm={12} md={12} lg={6}>				
					<Card bg="dark" text="light" className="my-3 mx-auto" style={{ width: '18rem' }}>
					  <Card.Body>
					    <Card.Title>Search Record</Card.Title>

					    <Form onSubmit = {e => searchCategory(e)}>
					    	<Form.Group>
					    		<Form.Label>Category</Form.Label>
					    		<Form.Control type="text" placeholder="Search via Category" value={category} onChange={e => setCategory(e.target.value)} required />
					    	</Form.Group>
						    <Button type="submit" className="btn-secondary btn">Search</Button>
						</Form>

						<Form onSubmit = {e => searchDescription(e)}>
					    	<Form.Group>
					    		<Form.Label>Description</Form.Label>
					    		<Form.Control type="text" placeholder="Search via Description" value={description} onChange={e => setDescription(e.target.value)} required />
					    	</Form.Group>
						    <Button type="submit" className="btn-secondary btn">Search</Button>
					    </Form>

						</Card.Body>
					</Card>
				</Col>

				<Col className="my-3">				
					<Table responsive="sm" size="md" striped bordered hover variant="dark" className="mx-auto">
						<thead>
							<tr className="text-center">
								<th>Type</th>
								<th>Category</th>
								<th>Description</th>
								<th>Amount</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							{recordRows}
						</tbody>
					</Table>
				</Col>

			</Row>
		</Fragment>
	)
}