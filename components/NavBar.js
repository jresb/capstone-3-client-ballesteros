import {useState, useContext, Fragment} from 'react'
import {Form,Button,Navbar,Nav,NavDropdown} from 'react-bootstrap'

import Link from 'next/link'

import UserContext from './../userContext'

export default function NavBar() {
	const {user} = useContext(UserContext)
	console.log(user)

	const [search,setSearch] = useState('')

	return (
		<Navbar bg="dark" variant="dark" expand="lg">
		  <Navbar.Brand href="/">Pence</Navbar.Brand>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		      {
		      	user.email
		      	?
		      	<Fragment>	      		
				    <Nav className="mr-auto">
				    	<Nav.Link href="/">Home</Nav.Link>
					    <Nav.Link href="/profile">Profile</Nav.Link>
					    <NavDropdown title="Actions" id="basic-nav-dropdown">
					    <NavDropdown.Item href="/records">Records</NavDropdown.Item>
					    <NavDropdown.Item href="/categories">Categories</NavDropdown.Item>
					    <NavDropdown.Item href="/balanceOverview">Balance Overview</NavDropdown.Item>
					    <NavDropdown.Divider />
					    <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
					    </NavDropdown>
					    <Nav.Link href="/about">About</Nav.Link>
					</Nav>
					    	<Button type="submit" variant="outline-secondary" href="/search">Search</Button>
		      	</Fragment>		
				:
				<Nav className="mr-auto">
					<Nav.Link href="/">Home</Nav.Link>
					<Nav.Link href="/login">Login</Nav.Link>
					<Nav.Link href="/register">Register</Nav.Link>
				    <Nav.Link href="/about">About</Nav.Link>
				</Nav>
		      }
		  </Navbar.Collapse>
		</Navbar>
	)
}