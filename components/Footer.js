export default function Footer(){

	return(

		<footer className="footer mt-2 fixed-bottom">
			<p className="p-3 text-center"> Jerico Ballesteros &copy; 2021</p>
		</footer>
	)
}